const locales = require("./src/locales");

let baseRoutes = [
  "/",
  '/guides/openstreetmap',
  '/guides/translate-framasoft',
  '/guides/gitlab-account',
  '/guides/weblate-account',
  '/guides/report-a-bug',
  '/guides/request-a-feature',
  '/guides/review-documentation',
  '/guides/patch-documentation',
  '/guides/framalibre-edit'
];
let finalRoutes = [];
baseRoutes.forEach(p => {
  finalRoutes.push(p);
  locales.locales.forEach(l => {
    finalRoutes.push("/" + l.code + p);
  });
});

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' && process.env.CI_PROJECT_NAME
      ? '/' + process.env.CI_PROJECT_NAME + '/'
      : '/',
  chainWebpack: (config) => {
    config.plugins.delete('prefetch');
  },

  pluginOptions: {
    prerenderSpa: {
      registry: undefined,
      renderRoutes: finalRoutes,
      useRenderEvent: true,
      headless: true,
      onlyProduction: true
    }
  }
};
