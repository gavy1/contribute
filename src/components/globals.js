import Vue from "vue";
import Icon from "./Icon";
import GuideMenu from "./guide/Menu";
import GuideContent from "./guide/Content";
import GuideStep from "./guide/Step";
import StepImage from "./guide/Image";
import Modal from "./Modal";

Vue.component('icon', Icon)
Vue.component('guide-menu', GuideMenu)
Vue.component('guide-content', GuideContent)
Vue.component('guide-step', GuideStep)
Vue.component('step-image', StepImage)
Vue.component('modal', Modal)

export default {}
