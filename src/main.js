import Vue from "vue"
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import App from "./App.vue";
import './setup'
import router from './router'

import VueHead from "vue-head";

Vue.use(VueHead, {
  separator: "-",
  complement: "Funkwhale"
});

Vue.use(Buefy, {
  defaultIconPack: 'fa'
})
require("bulma/css/bulma.css");
require('fork-awesome/css/fork-awesome.min.css')

new Vue({
  router,
  render: h => h(App),
  mounted: () => document.dispatchEvent(new Event("x-app-rendered")),
}).$mount("#app");
