import VueRouter from 'vue-router'

import Home from './views/Home'
import Guide from './views/Guide'

const routes = [
  {path: '/:locale?', component: Home, props: true,},
  {
    path: '/:locale?/guides/:slug',
    name: 'guide',
    props: true,
    component: Guide,
  }
]
export default new VueRouter({
  routes,
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior (to, from) {
    if (to.path === from.path) {
      // only anchor changed, we don't do anything
      return
    }
    return { x: 0, y: 0 }
  }
})
